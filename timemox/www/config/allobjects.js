$( document ).ready(function() { mox.config.objects = ([
{
	"id": 0
	,"name":	"Start"
	,"class":	"special"
	,"subclass":	"start"
	,"name_params":	["Save","valor2","valor3"]
	,"params":	[1,0,0]
	,"graph": 5
	,"angle": 0
},
{
	"id": 10
	,"name":	"Pentagram"
	,"class":	"special"
	,"subclass":	"pentagram"
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": 4
	,"angle": 0
}
,
{
	"id": 11
	,"name":	"Clay"
	,"class":	"special"
	,"subclass":	"clay"
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": -1
}
,
{
	"id": 12
	,"name":	"Axe"
	,"class":	"axe"
	,"subclass":"simple"
	,"move": "circular"	
	,"name_params":	["Direction","Offset","Long","Set"]
	,"params":	[0,0,48,0]
	,"damage":	[3,1,9]
	,"graph": [ [ 0, 64, 32, 96 ],[ 0, 96, 32, 128 ], [ 0, 96, 32, 128 ],[ 0, 128, 32, 160 ] ]
}
,
{
	"id": 13
	,"name":	"Double Axe"
	,"class":	"axe"
	,"subclass":"double"
	,"move": "circular"	
	,"name_params":	["Direction","Offset","Long","Set"]
	,"params":	[0,0,48,0]
	,"damage":	[3,1,9]
	,"graph": [ [ 0, 64, 32, 96 ],[ 0, 96, 32, 128 ], [ 0, 96, 32, 128 ],[ 0, 128, 32, 160 ] ]
}
,
{
	"id": 14
	,"name":	"Axe semicircle"
	,"class":	"axe"
	,"subclass":"simple"
	,"move": "semicircular"
	,"angle":	0
	,"name_params":	["Direction","Offset","Long","Set"]
	,"params":	[0,0,48,0]
	,"damage":	[3,1,9]	
	,"graph": [ [ 0, 64, 32, 96 ],[ 0, 96, 32, 128 ], [ 0, 96, 32, 128 ],[ 0, 128, 32, 160 ] ]
},
{
	"id": 15
	,"name":	"Axe semicircle"
	,"class":	"axe"
	,"subclass":"simple"
	,"move": "semicircular"
	,"angle":	180
	,"name_params":	["Direction","Offset","Long","Set"]
	,"params":	[0,0,48,0]
	,"damage":	[3,1,9]	
	,"graph": [ [ 0, 64, 32, 96 ],[ 0, 96, 32, 128 ], [ 0, 96, 32, 128 ],[ 0, 128, 32, 160 ] ]
},
{
	"id": 168
	,"name":	"Enemy 1"
	,"class":	"enemy"
	,"subclass":	"human"
	,"name_params":	["Respawn","Alert","Level"]
	,"params":	[50000,200,1]
	,"damage":	[3,0,15]
	,"life": 50
	,"attack": 28
	,"set": 0
},
{
	"id": 16
	,"name":	"Axe semicircle"
	,"class":	"axe"
	,"subclass":"simple"
	,"move": "semicircular"
	,"angle":	90
	,"name_params":	["Direction","Offset","Long","Set"]
	,"params":	[0,0,48,0]
	,"damage":	[3,1,9]	
	,"graph": [ [ 0, 64, 32, 96 ],[ 0, 96, 32, 128 ], [ 0, 96, 32, 128 ],[ 0, 128, 32, 160 ] ]
},
{
	"id": 17
	,"name":	"Axe semicircle"
	,"class":	"axe"
	,"subclass":"simple"
	,"move": "semicircular"
	,"angle":	270
	,"name_params":	["Direction","Offset","Long","Set"]
	,"params":	[0,0,48,0]
	,"damage":	[3,1,9]	
	,"graph": [ [ 0, 64, 32, 96 ],[ 0, 96, 32, 128 ], [ 0, 96, 32, 128 ],[ 0, 128, 32, 160 ] ]
},
{
	"id": 18
	,"name":	"Saw"
	,"class":	"saw"
	,"damage":	[3,1,9]
	,"name_params":	["Direction","Offset","Range","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 64, 32, 96 ],[ 128, 32, 160, 64 ],[ 128, 64, 160, 96 ]]
}
,
{
	"id": 19
	,"name":	"Saw wall"
	,"class":	"saw_wall"
	,"damage":	[3,1,9]
	,"name_params":	["Direction","Offset","Range","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 32, 32, 64 ],[ 128, 32, 160, 64 ],[ 128, 64, 160, 96 ]]
	,"angle": 0
}
,
{
	"id": 1
	,"name":	"Go down"
	,"class":	"special"
	,"subclass":	"go-down"
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": -1
}
,
{
	"id": 20
	,"name":	"Saw wall"
	,"class":	"saw_wall"
	,"damage":	[3,1,9]
	,"name_params":	["Direction","Offset","Range","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 32, 32, 64 ],[ 128, 32, 160, 64 ],[ 128, 64, 160, 96 ]]
	,"angle": 180	
}
,
{
	"id": 21
	,"name":	"Saw wall"
	,"class":	"saw_wall"
	,"damage":	[3,1,9]
	,"name_params":	["Direction","Offset","Range","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 32, 32, 64 ],[ 128, 32, 160, 64 ],[ 128, 64, 160, 96 ]]
	,"angle": 90	
}
,
{
	"id": 22
	,"name":	"Saw wall"
	,"class":	"saw_wall"
	,"damage":	[3,1,9]
	,"name_params":	["Direction","Offset","Range","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 32, 32, 64 ],[ 128, 32, 160, 64 ],[ 128, 64, 160, 96 ]]
	,"angle": 270	
}
,
{
	"id": 23
	,"name":	"Lava pit"
	,"class":	"tempor"
	,"subclass": "trap"
	,"name_params":	["Time","Offset","Retire","Set"]
	,"damage":	[3,3,9]	
	,"params":	[1,0,0,0]
	,"graph": [[ 128, 96, 160 , 128 ],[ 160, 96, 192 , 128 ],[ 192, 96, 224 , 128 ],[ 224, 96, 256 , 128 ]]
},
{
	"id": 24
	,"name":	"Spike right"
	,"class":	"tempor"
	,"subclass": "spike"
	,"name_params":	["Time","Offset","Retire","Set"]
	,"damage":	[15,0,9]	
	,"params":	[1,0,0,0]
	,"graph": [[ 128, 0, 160, 32 ],[ 128, 160 , 160, 192 ],[ 160, 160 , 192, 192 ],[ 192, 160, 224, 192 ],[ 224, 160 , 256, 192 ] ]
	,"angle": 0
},
{
	"id": 25
	,"name":	"Spike"
	,"class":	"tempor"
	,"subclass": "spike"	
	,"name_params":	["Time","Offset","Retire","Set"]
	,"damage":	[15,0,9]
	,"params":	[1,0,0,0]
	,"graph": [[ 128, 0, 160, 32 ],[ 128, 160 , 160, 192 ],[ 160, 160 , 192, 192 ],[ 192, 160, 224, 192 ],[ 224, 160 , 256, 192 ] ]
	,"angle": 180
}
,
{
	"id": 26
	,"name":	"Spike"
	,"class":	"tempor"
	,"subclass": "spike"	
	,"name_params":	["Time","Offset","Retire","Set"]
	,"damage":	[15,0,9]
	,"params":	[1,0,0,0]
	,"graph": [[ 128, 0, 160, 32 ],[ 128, 160 , 160, 192 ],[ 160, 160 , 192, 192 ],[ 192, 160, 224, 192 ],[ 224, 160 , 256, 192 ] ]
	,"angle": 90
}
,
{
	"id": 27
	,"name":	"Spike"
	,"class":	"tempor"
	,"subclass": "spike"	
	,"name_params":	["Time","Offset","Retire","Set"]
	,"damage":	[15,0,9]
	,"params":	[1,0,0,0]
	,"graph": [[ 128, 0, 160, 32 ],[ 128, 160 , 160, 192 ],[ 160, 160 , 192, 192 ],[ 192, 160, 224, 192 ],[ 224, 160 , 256, 192 ] ]
	,"angle": 270
}
,
{
	"id": 28
	,"name":	"Volcano"
	,"class":	"volcano"
	,"name_params":	["Pulse","Offset","Radius","Set"]
	,"params":	[1,0,32,0]
	,"damage":	[3,4,9]
	,"graph": [0,160,32,192]
},
{
	"id": 29
	,"name":	"Toxic cloud"
	,"class":	"toxic_cloud"
	,"name_params":	["Radius","Offset","Type","Set"]
	,"params":	[0,0,0,0]
	,"damage":	[0.005,4,1500]
	,"graph": [0,192,32,224]
},
{
	"id": 2
	,"name":	"Go up"
	,"class":	"special"
	,"subclass":	"go-up"
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": -1
},
{
	"id": 30
	,"name":	"Dart right"
	,"class":	"shot"
	,"subclass": 	"cross"
	,"damage":	[3,1,9]
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]	
	,"angle": 0
}
,
{
	"id": 31
	,"name":	"Dart left"
	,"class":	"shot"
	,"subclass":	"cross"
	,"damage":	[3,1,9]
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]		
	,"angle": 180
},
{
	"id": 32
	,"name":	"Dart up"
	,"class":	"shot"
	,"subclass":	"cross"
	,"damage":	[3,1,9]
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]	
	,"angle": 90	
}
,
{
	"id": 33
	,"name":	"Dart down"
	,"class":	"shot"
	,"subclass":	"cross"
	,"damage":	[3,1,9]
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]	
	,"angle": 270	
}
,
{
	"id": 34
	,"name":	"Dart down-right"
	,"class":	"shot"
	,"subclass": "cross"
	,"damage":	[3,1,9]
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]	
	,"angle": 315
}
,
{
	"id": 35
	,"name":	"Dart down-left"
	,"class":	"shot"
	,"subclass": "cross"
	,"damage":	[3,1,9]
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]	
	,"angle": 225
}
,
{
	"id": 36
	,"name":	"Flame right"
	,"class":	"shot"
	,"subclass": 	"flame"
	,"damage":	[3,1,9]
	,"name_params":	["Speed","Offset","Width","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]		
	,"angle": 0
}
,
{
	"id": 37
	,"name":	"Flame left"
	,"class":	"shot"
	,"subclass": "flame"
	,"damage":	[3,3,9]
	,"name_params":	["Speed","Offset","Width","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]	
	,"angle": 180
}
,
{
	"id": 3
	,"name":	"Go Teleport"
	,"class":	"special"
	,"subclass":	"teleport"
	,"name_params":	["X,Y,Map","Mapa","Mundo"]
	,"params":	[0,0,0]
	,"graph": 0	
	,"angle": 0	
}
,
{
	"id": 40
	,"name":	"Dart up-right"
	,"class":	"shot"
	,"subclass": "cross"
	,"damage":	[3,1,9]
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]	
	,"angle": 45
}
,
{
	"id": 41
	,"name":	"Dart up-left"
	,"class":	"shot"
	,"subclass": "cross"
	,"damage":	[3,1,9]
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
	,"graph": [[ 0, 224, 32, 256 ],[ 0, 256, 32, 288 ]]	
	,"angle": 135
}
,
{
	"id": 48
	,"name":	"Steps"
	,"class":	"steps"
	,"name_params":	["Time","Offset","Radius","Set"]
	,"params":	[0,0,0,0]
}
,
{
	"id": 4
	,"name":	"Health"
	,"class":	"special"
	,"subclass":	"health"
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": 1	
	,"angle": 0	
}
,
{
	"id": 54
	,"name":	"Light down-right"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": 315
},
{
	"id": 55
	,"name":	"Light down-left"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": 225
},
{
	"id": 56
	,"name":	"Light left"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": 0
},
{
	"id": 57
	,"name":	"Light right"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": 180
},
{
	"id": 58
	,"name":	"Light up"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": 90
},
{
	"id": 59
	,"name":	"Light down"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": 270
},
{
	"id": 5
	,"name":	"Mana"
	,"class":	"special"
	,"subclass":	"mana"
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": 2	
	,"angle": 0	
}
,
{
	"id": 60
	,"name":	"Light up-right"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": 45
},
{
	"id": 61
	,"name":	"Light up-left"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": 135
},
{
	"id": 62
	,"name":	"Light center"
	,"class":	"light"
	,"name_params":	["Opacity","Distance","Color"]
	,"params":	[30,50,"#FFF"]
	,"angle": -1
},
{
	"id": 66
	,"name":	"Spectre yellow"
	,"class":	"spectre"
	,"name_params":	["Respawn","Alert","Set"]
	,"params":	[0,0,0]
	,"damage":	[3,1,9]	
	,"set": 0
},
{
	"id": 67
	,"name":	"Spectre orange"
	,"class":	"spectre"
	,"name_params":	["Respawn","Alert","Set"]
	,"params":	[0,0,0]
	,"damage":	[3,1,9]	
	,"set": 1
},
{
	"id": 68
	,"name":	"Spectre green"
	,"class":	"spectre"
	,"name_params":	["Respawn","Alert","Set"]
	,"params":	[0,0,0]
	,"damage":	[3,1,9]	
	,"set": 2
},
{
	"id": 69
	,"name":	"Spectre red"
	,"class":	"spectre"
	,"name_params":	["Respawn","Alert","Set"]
	,"params":	[0,0,0]
	,"damage":	[3,1,9]	
	,"set": 3
},
{
	"id": 6
	,"name":	"Magnet path"
	,"class":	"special"
	,"subclass":	"magnet_path"
	,"delta":	0
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": 3
	,"angle": 0	
}
,
{
	"id": 70
	,"name":	"Spectre blue"
	,"class":	"spectre"
	,"name_params":	["Respawn","Alert","Set"]
	,"params":	[0,0,0]
	,"damage":	[3,1,9]	
	,"set": 4
},
{
	"id": 71
	,"name":	"Spectre purple"
	,"class":	"spectre"
	,"name_params":	["Respawn","Alert","Set"]
	,"params":	[0,0,0]
	,"damage":	[3,1,9]	
	,"set": 5
},
{	
	"id": 7
	,"name":	"Magnet path"
	,"class":	"special"
	,"subclass":	"magnet_path"
	,"delta":	1
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": 3	
	,"angle": 90	
}
,
{
	"id": 84
	,"name":	"Treasure"
	,"class":	"item"
	,"subclass":	"treasure"
	,"name_params":	["Respawn","Count","Set"]
	,"params":	[99999,20,0]
},
{
	"id": 85
	,"name":	"Armor"
	,"class":	"item"
	,"subclass":	"armor"
	,"name_params":	["Respawn","Count","Set"]
	,"params":	[99999,25,0]
},
{
	"id": 86
	,"name":	"Life"
	,"class":	"item"
	,"subclass":	"life"
	,"name_params":	["Respawn","Count","Set"]
	,"params":	[99999,25,0]
},
{
	"id": 87
	,"name":	"Mana"
	,"class":	"item"
	,"subclass":	"mana"
	,"name_params":	["Respawn","Count","Set"]
	,"params":	[99999,50,0]
},
{
	"id": 88
	,"name":	"Poison"
	,"class":	"item"
	,"subclass":	"poison"
	,"name_params":	["Respawn","Count","Set"]
	,"params":	[99999,50,0]
},
{
	"id": 8
	,"name":	"Magnet path"
	,"class":	"special"
	,"subclass":	"magnet_path"
	,"delta":	2
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": 3	
	,"angle": 180	
}
,
{
	"id": 9
	,"name":	"Magnet path"
	,"class":	"special"
	,"subclass":	"magnet_path"
	,"delta":	3
	,"name_params":	["valor1","valor2","valor3"]
	,"params":	[0,0,0]
	,"graph": 3	
	,"angle": 270	
}
]);})
